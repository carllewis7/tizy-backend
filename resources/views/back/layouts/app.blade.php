<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title', __('Dollar'))</title>
    <link rel="icon" href="{{_asset('favicon.ico')}}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/components/table.css"/>
    <link href="{{_asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{_asset('plugins/node-waves/waves.css')}}" rel="stylesheet"/>
    <link href="{{_asset('plugins/animate-css/animate.css')}}" rel="stylesheet"/>
    <link href="{{_asset('plugins/morrisjs/morris.css')}}" rel="stylesheet"/>
    <link href="{{_asset('css/style.css')}}" rel="stylesheet">
    <link href="{{_asset('plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet">
    <link href="{{_asset('plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <link href="{{_asset('css/themes/all-themes.css')}}" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    @yield('styles')
    <style>
        .alert {
            z-index: 99999999 !important;
        }
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #8888881f;
        }
    </style>
    <script>
        $urldomain = '{{env('URL_DOMAIN')}}';
    </script>
</head>
<body class="theme-indigo">
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<div class="overlay"></div>
@include('back.parts.top')
<section>
    @include('back.parts.sidebar_left')
    @include('back.parts.sidebar_right')
</section>
<section class="content">
    <div class="container-fluid">
        @yield('content')
    </div>
</section>
<script src="{{_asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{_asset('plugins/bootstrap/js/bootstrap.js')}}"></script>
{{--<script src="{{_asset('plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>--}}
<script src="{{_asset('plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{_asset('plugins/node-waves/waves.js')}}"></script>
<script src="{{_asset('plugins/jquery-countto/jquery.countTo.js')}}"></script>
<script src="{{_asset('plugins/raphael/raphael.min.js')}}"></script>
<script src="{{_asset('plugins/morrisjs/morris.js')}}"></script>
<script src="{{_asset('plugins/chartjs/Chart.bundle.js')}}"></script>
{{--<script src="{{_asset('plugins/flot-charts/jquery.flot.js')}}"></script>--}}
{{--<script src="{{_asset('plugins/flot-charts/jquery.flot.resize.js')}}"></script>--}}
{{--<script src="{{_asset('plugins/flot-charts/jquery.flot.pie.js')}}"></script>--}}
{{--<script src="{{_asset('plugins/flot-charts/jquery.flot.categories.js')}}"></script>--}}
{{--<script src="{{_asset('plugins/flot-charts/jquery.flot.time.js')}}"></script>--}}
<script src="{{_asset('plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>
<script src="{{_asset('plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.js"></script>
<script src="{{_asset('plugins/dropzone/dropzone.js')}}"></script>
<script src="{{_asset('plugins/bootstrap-notify/bootstrap-notify.js')}}"></script>
<script src="{{_asset('js/admin.js')}}"></script>
<script src="{{_asset('js/pages/index.js')}}"></script>
<script src="{{_asset('js/demo.js')}}"></script>
<script src="{{_asset('plugins/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{_asset('plugins/jquery-qrcode-0.14.0.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="{{_asset('js/Generic/generic.js')}}"></script>
<script src="{{_asset('js/Generic/typeahead.bundle.js')}}"></script>
@yield('scripts')
@if(isset($status))
    <script type="text/javascript">
        var title = '{{$status}}';
        if (title === 'OK') {
            $.notify({message: '{{$message}}'}, {delay: 2000,type: 'success'});
        } else if (title === 'FAILED') {
            $.notify({message: '{{$message}}'}, {delay: 2000,type: 'danger'});
        }
    </script>
@endif
@if(count($errors) > 0)
    <script>
        $(function () {
            @foreach($errors->all() as $error)
                       $.notify({message: '{{ $error }}'}, {delay: 2000,type: 'danger'});
            @endforeach
        });
    </script>
@endif
@if(session()->has('status'))
    @if (session()->get('status') == 'OK')
        <script>
            $(function () {
                $.notify({message: '{!! session()->get('message') !!}'}, {delay: 2000,type: 'success'});
            });
        </script>
    @endif
    @if (session()->get('status') == 'FAILED')
        <script>
            $(function () {
                $.notify({message: '{!! session()->get('message') !!}'}, {delay: 2000,type: 'danger'});
            });
        </script>
    @endif
@endif
</body>
</html>