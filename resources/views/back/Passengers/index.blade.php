@extends('back.layouts.app')
@section('title') Pasajeros @endsection
@section('passenger.title', 'active')
@section('content')
    <style>
         .centrado{
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
        }
    </style>
    <div class="block-header">
    <!-- <h2>{{__('Pasajeros')}}</h2>-->
    </div>
    <div class="row clearfix" id="main">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>{{__('Pasajeros Registrados')}}</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <table id="PassengersTable" class="table table-bordered dataTable">
                        <thead>
                        <tr>
                            <th>{{__('Nombre y Apellido')}}</th>
                            <th>{{__('Cedula')}}</th>
                            <th>{{__('Género')}}</th>
                            <th>{{__('Telefono')}}</th>
                            <th>{{__('Correo')}}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <div id="ModalDelivery" tabindex="-1" role="dialog" aria-hidden="true" class="modal text-left">
            <div role="document" class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{_('Detalles de Pasajero')}}</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="height: 345px;">
                            <form novalidate="novalidate" autocomplete="off" style="height: 100%;">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label for="">Nombre</label>
                                                <input type="text" readonly v-model="name" placeholder="Descripción de envio"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <div class="form-line">
                                                <label for="">Cedula</label>
                                                <input type="text" readonly v-model="dni"  placeholder="Descripción de envio"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <div class="form-line">
                                                <label for="">Telefono</label>

                                                <input type="text" readonly
                                                       v-model="phone"
                                                       placeholder="Nombre del cliente"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <div class="form-line">
                                                <label for="">Genero</label>

                                                <input type="text" readonly
                                                       v-model="gendertext"
                                                       placeholder="Nombre del cliente"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="height:100%;">
                                    <div id="qr" class="centrado"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/Passenger/passengers.js')}}"></script>
@endsection
