@extends('back.layouts.app')
@section('title') Principal @endsection
@section('principal.admin.title', 'active')
@section('content')
   <script>
        $tokenuser = '{{\Auth::user()->token}}';
    </script>
    <div class="block-header">
        <h2>Principal</h2>
    </div>
    <div class="row clearfix" id="welcome">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>Bienvenido {{\Auth::user()->name}}</h2>
                        </div>
                       
                    </div>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                           
                        </li>
                    </ul>
                </div>
                <div class="body">
               
                <h5 style='font-size:22px'>Tu Saldo Actual es:</h5>               
                <div style="font-size:62px">
                @{{fd.Balance}}
                </div>
               
              
                
                
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@section('scripts')

<script src="{{asset('js/Forms/welcome.js')}}"></script>
@endsection
