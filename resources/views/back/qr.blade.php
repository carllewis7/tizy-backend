    <style>
         .centrado{
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
        }
    </style>
    <script src="{{_asset('plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{_asset('plugins/jquery-qrcode-0.14.0.min.js')}}"></script>
 <div id="qr" class="centrado"></div>
    <script>
        $option = {
            render: 'div',
            minVersion: 1,
            maxVersion: 40,
            ecLevel: 'H',
            left: 0,
            top: 0,
            size: 300,
            fill: '#000',
            background: null,
            text: '{{request()->route('token')}}',
            radius: 0,
            quiet: 0,
            mode: 2,
            mSize: 0.1,
            mPosX: 0.5,
            mPosY: 0.5,
            label: 'Tizy',
            fontname: 'sans',
            fontcolor: '#000',
            image: null
        };
        $('#qr').empty().qrcode($option);
    </script>


