<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Registro</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="../../css/sweetalert2.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="../../css/style.css" rel="stylesheet">
     <script>
        $urldomain = '{{env('URL_DOMAIN')}}';
    </script>
</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);">Registro</a>
         <!--    <small>Admin BootStrap Based - Material Design</small> -->
        </div>
        <div class="card" id="vueregister">
            <div class="body">
                
                    <div class="msg">Registro de Nueva Cuenta</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" v-model='fd.name' class="form-control" name="namesurname" placeholder="Nombre Y Apellido" >
                        </div>
                    </div>
                     <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">picture_in_picture_alt</i>
                        </span>
                        <div class="form-line">
                            <input type="number" v-model='fd.dni' class="form-control" name="dni" placeholder="Cédula" >
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">picture_in_picture_alt</i>
                        </span>
                        <div class="form-line">
                            <input type="phone" v-model='fd.phone' class="form-control" name="dni" placeholder="Telefono" >
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">picture_in_picture_alt</i>
                        </span>
                        <div class="form-line">
                        <select v-model="fd.gender" class="form-control">
                                            <option value="1">Masculino</option>
                                            <option value="2">Femenino</option>
                                        </select> </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input v-model='fd.email' type="email" class="form-control" name="email" placeholder="Correo" >
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" v-model='fd.password1' class="form-control" name="password" minlength="6" placeholder="Contraseña" >
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" v-model='fd.password2' class="form-control" name="confirm" minlength="6" placeholder="Confirmar Contraseña" >
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                        <label for="terms">Acepto los Términos <br><a href="javascript:void(0);">Términos de Uso</a>.</label>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" v-on:click="guardar">Regístrate</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="sign-in.html">Ya Tienes un Usuario?</a>
                    </div>
                
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../../plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="../../plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>
    <script src="../../js/pages/examples/sign-up.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script src="../js/sweetalert2.js"></script>
    <script src="https://unpkg.com/promise-polyfill"></script>

      <script src="../js/Forms/register.js"></script>
</body>

</html>