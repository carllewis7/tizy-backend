<?php
Route::group([
    'middleware' => [
        'auth',
    ],
    'prefix' => '/register',
], function () {

    Route::get('', [
        'middleware' => ['permission:' . \App\Security\Enums\Permissions::$login,],
        'as' => 'back.passenger.index',
        'uses' => 'Back\UserController@register',
    ]);

});