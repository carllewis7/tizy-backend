<?php
    Route::group([
        'middleware' => [
            'auth',
        ],
        'prefix'     => '/',
    ], function () {
        
        Route::get('', [
//            'middleware' => ['permission:' . \App\Security\Enums\Permissions::$login,],
            'as'         => 'back.index',
            'uses'       => 'Back\Controller@home',
        ]);
        
    });

