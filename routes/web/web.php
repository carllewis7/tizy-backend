<?php
    Route::group([
        'middleware' => [
//            'auth',
        ],
        'prefix'     => '/',
    ], function () {
        
        Route::get('', [
//            'middleware' => ['permission:' . \App\Security\Enums\Permissions::$login,],
            'as'         => 'web.index',
            'uses'       => 'Web\Controller@index',
        ]);
        
    });

