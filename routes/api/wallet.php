<?php
    
    use Illuminate\Http\Request;

    Route::group([
        'middleware' => [
            'api',
        ],
        'prefix'     => 'wallet',
    ], function () {
        
        Route::get('/', [
            'as'   => 'api.wallet',
            'uses' => 'Api\WalletController@index',
        ]);
        
        Route::post('/', [
            'as'   => 'api.wallet.store',
            'uses' => 'Api\WalletController@store',
        ]);
        
        Route::get('/{token}', [
            'as'   => 'api.wallet.get',
            'uses' => 'Api\WalletController@get',
        ]);

        Route::get('/user/{token}', [
            'as'   => 'api.wallet.user.get',
            'uses' => 'Api\WalletController@getByTokenUser',
        ]);

        Route::put('/{token}', [
            'as'   => 'api.wallet.update',
            'uses' => 'Api\WalletController@update',
        ]);
    
        Route::delete('/{token}', [
            'as'   => 'api.wallet.delete',
            'uses' => 'Api\WalletController@delete',
        ]);
        
    });
