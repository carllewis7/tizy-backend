<?php
    
    use Illuminate\Http\Request;

    Route::group([
        'middleware' => [
            'api',
        ],
        'prefix'     => 'transaction',
    ], function () {
        
        Route::get('/', [
            'as'   => 'api.transaction',
            'uses' => 'Api\TransactionController@index',
        ]);
        
        Route::post('/', [
            'as'   => 'api.transaction.store',
            'uses' => 'Api\TransactionController@store',
        ]);
        
        Route::get('/{token}', [
            'as'   => 'api.transaction.get',
            'uses' => 'Api\TransactionController@get',
        ]);
        
        Route::put('/{token}', [
            'as'   => 'api.transaction.update',
            'uses' => 'Api\TransactionController@update',
        ]);
    
        Route::delete('/{token}', [
            'as'   => 'api.transaction.delete',
            'uses' => 'Api\TransactionController@delete',
        ]);
        
    });
