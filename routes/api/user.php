<?php
    
    use Illuminate\Http\Request;

    Route::group([
        'middleware' => [
            'api',
        ],
        'prefix'     => 'user',
    ], function () {
        
        Route::get('/', [
            'as'   => 'api.user',
            'uses' => 'Api\UserController@index',
        ]);
        
        Route::post('/', [
            'as'   => 'api.user.store',
            'uses' => 'Api\UserController@store',
        ]);
        
        Route::get('/{token}', [
            'as'   => 'api.user.get',
            'uses' => 'Api\UserController@get',
        ]);
        Route::get('/qr/{token}', [
            'as'   => 'api.get.qr',
            'uses' => 'Back\UserController@qr',
        ]);
        
        Route::put('/{token}', [
            'as'   => 'api.user.update',
            'uses' => 'Api\UserController@update',
        ]);
    
        Route::delete('/{token}', [
            'as'   => 'api.user.delete',
            'uses' => 'Api\UserController@delete',
        ]);
        
    });
