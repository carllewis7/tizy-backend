var main = new Vue({
    el: '#main',
    data: {
        token: '',
        phone: '',
        email: '',
        gender: '',
        gendertext: '',
        name: '',
        dni: ''
    },
    methods: {}
});

CargarTabla();

function CargarTabla() {
    var table = $("#PassengersTable").DataTable({
        "destroy": true,
        "rowCallback": function (row, data, index) {
            if (data.gender === 1) {
                data.gendertext = 'Masculino';
                $('td:eq(2)', row).html('<b>Masculino</b>');

            } else {
                data.gendertext = 'Femenino';
                $('td:eq(2)', row).html('<b>Femenino</b>');
            }
        },
        "columns": [
            {data: 'name'},
            {data: 'dni'},
            {data: 'gender'},
            {data: 'phone'},
            {data: 'email'}
        ],
        "ajax": {
            url: 'http://api.' + $urldomain + '/passenger',
            dataSrc: 'data'
        }
    });

    $option = {
        render: 'div',
        minVersion: 1,
        maxVersion: 40,
        ecLevel: 'H',
        left: 0,
        top: 0,
        size: 300,
        fill: '#000',
        background: null,
        text: 'no text',
        radius: 0,
        quiet: 0,
        mode: 2,
        mSize: 0.1,
        mPosX: 0.5,
        mPosY: 0.5,
        label: 'Tizy',
        fontname: 'sans',
        fontcolor: '#000',
        image: null
    };

    $('#PassengersTable tbody').on('click', 'tr', function () {

        main.token = table.row(this).data().token;
        main.phone = table.row(this).data().phone;
        main.email = table.row(this).data().email;
        main.gender = table.row(this).data().gender;
        main.gendertext = table.row(this).data().gendertext;
        main.name = table.row(this).data().name;
        main.dni = table.row(this).data().dni;
        $option.text =  main.token;
        $('#qr').empty().qrcode($option);
        $('#ModalDelivery').modal('show');
    });
}