function CheckNulls(items,strings)
{

    for (i = 0; i < items.length; i++) {
        if(items[i] === null || items[i] === "") {
            //toastr.warning('Por favor indica ' + strings[i]);
            $.notify({message: 'Porfavor indica ' + strings[i]}, {delay: 2000, type: 'warning'});
            return 0;
        }
    }
}