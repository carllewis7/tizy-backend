<?php
    
    namespace App\Client\Entities;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Client extends Model {

        protected $table      = 'Client';
        protected $primaryKey = 'id';
        
        protected $fillable = [
            'id',         
            'token',
            'name',
            'phone',         
            'clientcompany',
            'geolocation',            
            'email',            
            'others',
            'active',
            'status',
        ];
        public function scopeToken($query, $value) {
            return $query->where('token', $value)->first();
        }
    }
