<?php
    
    namespace App\Core\Entities;
    
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Database\Eloquent\Model;
    
    class Session extends Model {
        use Notifiable;
        protected $table        = 'sessions';
        protected $connection   = 'Totus_Food';
        protected $primaryKey   = 'id';
        public    $incrementing = false;
        public    $timestamps   = false;
        protected $fillable     = [
            'id',
            'user_id',
            'ip_address',
            'user_agent',
            'payload',
            'last_activity',
        ];
        
    }
