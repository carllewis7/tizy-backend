<?php

    namespace App\Core\Entities;

    use Illuminate\Database\Eloquent\Model;

    class Country extends Model {
     
        protected $table = 'countries';
        
        protected $connection = 'Totus_Food';

        protected $primaryKey = 'id';
        
        public $timestamps = false;
        
        public $incrementing = false;
        
        protected $fillable = [
            'id',
            'iso',
            'name',
            'dni',
        ];
        
        public function getFullNameAttribute() {

            return $this->iso . ' - ' . $this->name;
        }

        public function setDniAttribute($value) {

            $this->attributes['dni'] = json_encode($value);
        }

        public function getDniAttribute($value) {

            return json_decode($value);
        }

    }
