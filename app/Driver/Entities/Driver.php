<?php
    
    namespace App\Driver\Entities;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Driver extends Model {

        protected $table      = 'driver';
        protected $primaryKey = 'id';
        
        protected $fillable = [
            'id',         
            'iduser',
            'idasociation',
            'vehicle',
            'description',
            'active',
            
        ];
        public function scopeToken($query, $value) {
            return $query->where('token', $value)->first();
        }
    }
