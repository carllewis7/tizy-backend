<?php

    namespace App\Driver\Repositories;
    
    use App\Driver\Entities\Driver;
    use App\Users\Entities\User;
    use App\Asociation\Entities\Asociation;

    class DriverRepo {
    
        public function all() {

            $Driver = Driver::all();  
            foreach($Driver as $dri)
            {
                $dri->user = User::find($dri->iduser);
            }
            foreach($Driver as $dri)
            {
                $dri->asociation = Asociation::find($dri->idasociation);
            }
            

            return $Driver;
        }
  
        public function find($id) {
    
            $Driver = Driver::find($id);

            return $Driver;
        }  
        
        public function findByToken($token) {
            $Driver = Driver::Token($token);
         
            return $Driver;
        }      

        public function store($data) {
    
            $Driver = new Driver();
            $Driver->fill($data);
            $Driver->save();

            return $Driver;
        }
        
        public function update($Driver, $data) {
    
            $Driver->fill($data);
            $Driver->save();

            return $Driver;
        }
        public function delete($Driver,$data) {

            $Driver->active = false;
            $Driver->save();

            return $Driver;
        }
    }
