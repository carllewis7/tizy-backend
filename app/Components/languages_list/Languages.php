<?php

    namespace App\Components\languages_list;

    use App\Components\Helper;

    class Languages extends \App\Http\Controllers\Controller {
        public static function locales() {

            return Helper::array_object([
                'es_ES' => [
                    'flag' => _asset('img/flags/es.png'),
                    'name' => 'Español',
                ],
                'en_US' => [
                    'flag' => _asset('img/flags/us.png'),
                    'name' => 'English',
                ],
            ]);
        }
        
    }