<?php
namespace App\Components\sidebar;

use App\Http\Controllers\Back\Controller;
use App\Components\Helper;

class items extends Controller
{
    public static function sidebar()
    {
        return array_merge(
            Helper::sidebar('dashboard'),
            Helper::sidebar('passengers')
        );
    }
}
