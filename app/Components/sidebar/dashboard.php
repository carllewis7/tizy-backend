<?php
    
    return [
        'Principal Admin Title' => [
            'id'         => 'principal.admin.title',
            'icon'       => 'home',
            'url'        => 'back.index',
            'url_params' => [],
            'permission' => \App\Security\Enums\Permissions::$login,
            'type_user'  => null,
            'slug'       => null,
            'trans'      => __('Principal'),
            'subMenu'    => [],
        ],
    ];
