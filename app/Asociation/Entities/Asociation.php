<?php
    
    namespace App\Asociation\Entities;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Asociation extends Model {

        protected $table      = 'asociation';
        protected $primaryKey = 'id';
        
        protected $fillable = [
            'id',         
            'token',
            'name',
            'description',
            'active',
            
        ];
        public function scopeToken($query, $value) {
            return $query->where('token', $value)->first();
        }
    }
