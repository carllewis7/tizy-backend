<?php

    namespace App\Asociation\Repositories;
    
    use App\Asociation\Entities\Asociation;
   

    class AsociationRepo {
    
        public function all() {

            $Asociation = Asociation::all();       

            return $Asociation;
        }
  
        public function find($id) {
    
            $Asociation = Asociation::find($id);

            return $Asociation;
        }  
        
        public function findByToken($token) {
            $Asociation = Asociation::Token($token);
         
            return $Asociation;
        }      

        public function store($data) {
    
            $Asociation = new Asociation();
            $Asociation->fill($data);
            $Asociation->save();

            return $Asociation;
        }
        
        public function update($Asociation, $data) {
    
            $Asociation->fill($data);
            $Asociation->save();

            return $Asociation;
        }
        public function delete($Asociation,$data) {

            $Asociation->active = false;
            $Asociation->save();

            return $Asociation;
        }
    }
