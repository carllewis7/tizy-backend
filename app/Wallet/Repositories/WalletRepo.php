<?php

    namespace App\Wallet\Repositories;
    
    use App\Wallet\Entities\Wallet;
   

    class WalletRepo {
    
        public function all() {

            $Wallet = Wallet::all();       

            return $Wallet;
        }
  
        public function find($id) {
    
            $Wallet = Wallet::find($id);

            return $Wallet;
        }  
        public  function findbyclient($iduser) {
    
            $Wallet = Wallet::where('iduser',$iduser)->first();

            return $Wallet;
        } 
        
        public function findByToken($token) {
            $Wallet = Wallet::Token($token);
         
            return $Wallet;
        }      

        public function store($data) {
    
            $Wallet = new Wallet();
            $Wallet->fill($data);
            $Wallet->save();

            return $Wallet;
        }
        
        public function update($Wallet, $data) {
    
            $Wallet->fill($data);
            $Wallet->save();

            return $Wallet;
        }
        public function delete($Wallet,$data) {

            $Wallet->active = false;
            $Wallet->save();

            return $Wallet;
        }
    }
