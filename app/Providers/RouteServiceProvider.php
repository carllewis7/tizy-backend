<?php
    
    namespace App\Providers;
    
    use Illuminate\Support\Facades\Route;
    use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
    
    class RouteServiceProvider extends ServiceProvider {
        
        protected $namespace = 'App\Http\Controllers';
        
        public function boot() {
            parent::boot();
        }
        
        public function map() {
            $this->mapWebRoutes();
            $this->mapDomainRoutes();
            $this->mapApiRoutes();
            $this->mapBackRoutes();
        }
        
        protected function mapBackRoutes() {
            Route::group([
                'middleware' => ['web'],
                'domain'     => 'back.' . env('URL_DOMAIN', 'gurudevs.co'),
                'namespace'  => $this->namespace,
            ], function ($router) {
                self::get_routes(base_path('routes/back/'));
            });
        }
        
        protected function mapApiRoutes() {
            Route::group([
                'middleware' => [
                    'api',
                    'cors',
                ],
                'domain'     => 'api.' . env('URL_DOMAIN', 'gurudevs.co'),
                'namespace'  => $this->namespace,
            ], function ($router) {
                self::get_routes(base_path('routes/api/'));
            });
        }
        
        protected function mapWebRoutes() {
            Route::group([
//                'middleware' => ['web'],
                'domain'     => 'www.' . env('URL_DOMAIN', 'gurudevs.co'),
                'namespace'  => $this->namespace,
            ], function ($router) {
                self::get_routes(base_path('routes/web/'));
            });
        }
        protected function mapDomainRoutes() {
            Route::group([
                //                'middleware' => ['web'],
                'domain'     =>  env('URL_DOMAIN', 'gurudevs.co'),
                'namespace'  => $this->namespace,
            ], function ($router) {
                self::get_routes(base_path('routes/web/'));
            });
        }
        
        public function get_routes($dir) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if (!is_dir($dir . $file) && $file != "." && $file != "..") {
                        require $dir . $file;
                    } elseif ($file != "." && $file != "..") {
                        self::get_routes($dir . $file . '/');
                    }
                }
                closedir($dh);
            }
        }
    }
