<?php
    
    namespace App\Http\Controllers\Api;
    use Illuminate\Http\Request;
    use Illuminate\Routing\Controller as BaseController;
    
    class RolesController extends BaseController {

        public function store(Request $request){

            $response = [
                'status' => 'OK',
                'message' => __('Rol Creado Correctamente'),
                'data' => [],
            ];
            return response()->json($response);

        }

        public function update(Request $request){
            $response = [
                'status' => 'OK',
                'message' => __('Rol Actualizado Correctamente'),
                'data' => [],
            ];
            return response()->json($response);

        }

        public function delete(Request $request){
            $response = [
                'status' => 'OK',
                'message' => __('Rol Eliminado Correctamente'),
                'data' => [],
            ];
            return response()->json($response);

        }

        public function get(Request $request){
            $response = [
                'status' => 'OK',
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => [],
            ];
            return response()->json($response);

        }
    }
