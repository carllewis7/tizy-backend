<?php
    
    namespace App\Http\Controllers\Api;
    use Illuminate\Http\Request;
    use Illuminate\Routing\Controller as BaseController;
    
    class PermissionController extends BaseController {

        public function store(Request $request){
            $response = [
                'status' => 'OK',
                'message' => __('Permiso Creado Correctamente'),
                'data' => [],
            ];
            return response()->json($response);
        }

        public function update(Request $request){
            $response = [
                'status' => 'OK',
                'message' => __('Permiso Actualizado Correctamente'),
                'data' => [],
            ];
            return response()->json($response);
        }

        public function delete(Request $request){
            $response = [
                'status' => 'OK',
                'message' => __('Permiso Eliminado Correctamente'),
                'data' => [],
            ];
            return response()->json($response);
        }

        public function get(Request $request){
            $response = [
                'status' => 'OK',
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => [],
            ];
            return response()->json($response);
        }
    }
