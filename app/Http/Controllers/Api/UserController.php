<?php

namespace App\Http\Controllers\Api;

use App\Components\UUID;
use App\Security\Enums\Roles;
use App\Users\Repositories\UserRepo;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
class UserController extends BaseController
{

    private $UserRepo;

    public function __construct(UserRepo $UserRepo)
    {

        $this->UserRepo = $UserRepo;
    }

    public function index()
    {

        try {
            $users = $this->UserRepo->all();

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $users,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function passengers()
    {

        try {
            $users = $this->UserRepo->passengers();

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $users,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function get($token)
    {

        try {
            $user = $this->UserRepo->find_token($token);

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'dni' => 'required',
            'gender' => 'required',
            'password' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ], $this->custom_message());

        if ($validator->fails()) {

            $response = [
                'status' => 'FAILED',
                'code' => 400,
                'message' => __('Parametros incorrectos'),
                'data' => $validator->errors()->getMessages(),
            ];

            return response()->json($response, 400);
        }

        try {
            if(!$request->has('rol')){
                $request['rol'] = Roles::$passenger;
            }
            $request['token'] = UUID::v4();
            $request['active'] = 1;
            $request['status'] = 1;
            $user = $this->UserRepo->store($request->all());
            $response = [
                'status' => 'OK',
                'code' => '200',
                'message' => __('Usuario Creado Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            Log::error($ex);
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function update($token, Request $request)
    {

        try {
            $data = [];
            if($request->has('name')){
                $data['name'] = $request->get('name');
            }
            if($request->has('phone')){
                $data['phone'] = $request->get('phone');
            }
            if($request->has('password')){
                $data['password'] = $request->get('password');
            }
            if($request->has('email')){
                $data['email'] = $request->get('email');
            }
            if($request->has('dni')){
                $data['dni'] = $request->get('dni');
            }
            if($request->has('gender')){
                $data['gender'] = $request->get('gender');
            }
            if($request->has('rol')){
                $data['rol'] = $request->get('rol');
            }

            $user = $this->UserRepo->find_token($token);
            $user = $this->UserRepo->update($user, $request->all());

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Actualizado Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }
    }

    public function delete($token)
    {

        try {
            $user = $this->UserRepo->find_token($token);
            $user = $this->UserRepo->delete($user, ['active' => false]);
            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Eliminado Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function custom_message()
    {

        return [
            'name.required' => __('El nombre es requerido') . '.',
            'username.required' => __('El nombre de usuario es requerido') . '.',
            'token.required' => __('El token es requerido') . '.',
        ];

    }

    public function qr($token){
        return view('qr');
    }
}
