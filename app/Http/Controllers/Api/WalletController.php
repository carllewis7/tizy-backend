<?php

namespace App\Http\Controllers\Api;

use App\Components\UUID;
use App\Users\Repositories\UserRepo;
use App\Wallet\Repositories\WalletRepo;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class WalletController extends BaseController
{

    private $WalletRepo;
    private $UserRepo;

    public function __construct(WalletRepo $WalletRepo,UserRepo $UserRepo)
    {
        $this->WalletRepo = $WalletRepo;
        $this->UserRepo = $UserRepo;
    }

    public function index()
    {

        try {
            $users = $this->WalletRepo->all();

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $users,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            log::error($ex);
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function getByTokenUser($token){


        try {
            $user = $this->UserRepo->find_token($token);
            $wallet = $this->WalletRepo->findbyclient($user->id);

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $wallet,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }
    }

    public function get($token)
    {

        try {
            $user = $this->WalletRepo->findByToken($token);

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           
            'iduser' => 'required',
            'balance' => 'required',
         
        ], $this->custom_message());

        if ($validator->fails()) {

            $response = [
                'status' => 'FAILED',
                'code' => 400,
                'message' => __('Parametros incorrectos'),
                'data' => $validator->errors()->getMessages(),
            ];

            return response()->json($response, 400);
        }

        try {
            $request['token'] = UUID::v4();
            $data = [                       
                'token' => UUID::v4(),
                'iduser' => $request->get('iduser'),
                'balance' => $request->get('balance'),                      
               
                'active' => 1,
                'status' => 1,
            ];
            $user = $this->WalletRepo->store($data);
            $response = [
                'status' => 'OK',
                'code' => '200',
                'message' => __('Wallet Creado Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            log::error($ex);
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function update($token, Request $request)
    {

        try {
            $data = [];
            if($request->has('name')){
                $data['name'] = $request->get('name');
            }
            if($request->has('phone')){
                $data['phone'] = $request->get('phone');
            }
            if($request->has('password')){
                $data['password'] = $request->get('password');
            }
            if($request->has('email')){
                $data['email'] = $request->get('email');
            }
            if($request->has('dni')){
                $data['dni'] = $request->get('dni');
            }
            if($request->has('gender')){
                $data['gender'] = $request->get('gender');
            }
            if($request->has('rol')){
                $data['rol'] = $request->get('rol');
            }

            $user = $this->WalletRepo->find_token($token);
            $user = $this->WalletRepo->update($user, $request->all());

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Actualizado Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }
    }

    public function delete($token)
    {

        try {
            $user = $this->WalletRepo->find_token($token);
            $user = $this->WalletRepo->delete($user, ['active' => false]);
            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Eliminado Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function custom_message()
    {

        return [
            'name.required' => __('El nombre es requerido') . '.',
            'username.required' => __('El nombre de usuario es requerido') . '.',
            'token.required' => __('El token es requerido') . '.',
        ];

    }

}
