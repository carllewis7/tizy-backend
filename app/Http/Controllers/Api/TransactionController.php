<?php

namespace App\Http\Controllers\Api;

use App\Components\UUID;
use App\Transaction\Repositories\TransactionRepo;
use App\Users\Repositories\UserRepo;
use App\Wallet\Repositories\WalletRepo;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class TransactionController extends BaseController
{

    private $Transaction;
    private $Wallet;
    private $User;

    public function __construct(TransactionRepo $Transaction, WalletRepo $Wallet,UserRepo $User)
    {
        $this->Transaction = $Transaction;
        $this->Wallet = $Wallet;    
        $this->User = $User;
    }

    public function index()
    {

        try {
            $users = $this->Transaction->all();

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $users,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            log::error($ex);
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function get($token)
    {

        try {
            $user = $this->Transaction->findByToken($token);

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Datos Obtenidos Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);

        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'passenger' => 'required',
            'driver' => 'required',
            'amount' => 'required',
        ], $this->custom_message());

        if ($validator->fails()) {

            $response = [
                'status' => 'FAILED',
                'code' => 400,
                'message' => __('Parametros incorrectos'),
                'data' => $validator->errors()->getMessages(),
            ];

            return response()->json($response, 400);
        }

        try {

            $passenger = $this->User->find_token($request->get('passenger'));
            $driver = $this->User->find_token($request->get('driver'));

           //Transaccion de Debito
            $data = [                       
                'token' => UUID::v4(),
                'iduserA' => $passenger->id,
                'iduserB' => $driver->id,
                'amount' => floatval($request->get('amount')),
                'type' => 1,
                'active' => 1,
                'status' => 1,
            ];
            $user = $this->Transaction->store($data);
            //Transaccion de Credito
            $data2 = [                       
                'token' => UUID::v4(),
                'iduserA' => $driver->id,
                'iduserB' => $passenger->id,
                'amount' => $request->get('amount'),
                'type' => 2,
                'active' => 1,
                'status' => 1,
            ];
            $user = $this->Transaction->store($data2);
            $dataA = [];
            $dataB = [];
            log::debug('==== TEST ===='.$passenger->id);
            $walletA = $this->Wallet->findbyclient($passenger->id);
            if(!isset($walletA->id)){
                $data = [
                    'token' => UUID::v4(),
                    'iduser' => $driver->id,
                    'balance' => 0.00,
                    'active' => 1,
                ];
                $walletA = $this->Wallet->store($data);
            }
            $walletB = $this->Wallet->findbyclient($driver->id);
            if(!isset($walletB->id)){
                $data = [
                    'token' => UUID::v4(),
                    'iduser' => $passenger->id,
                    'balance' => 0.00,
                    'active' => 1,
                ];
                $walletA = $this->Wallet->store($data);
            }
            log::debug('==== TEST ===='.$request->get('iduserA'));
            log::debug($walletA);
            $dataA['balance'] = floatval($walletA->balance) - floatval($request->get('amount'));
            $dataB['balance'] = floatval($walletB->balance) + floatval($request->get('amount'));
            $userA = $this->Wallet->update($walletA, $dataA);
            $userB = $this->Wallet->update($walletB, $dataB);
            $response = [
                'status' => 'OK',
                'code' => '200',
                'message' => __('Transaccion creada Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            Log::error($ex);
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function update($token, Request $request)
    {

        try {
            $data = [];
            if($request->has('name')){
                $data['name'] = $request->get('name');
            }
            if($request->has('phone')){
                $data['phone'] = $request->get('phone');
            }
            if($request->has('password')){
                $data['password'] = $request->get('password');
            }
            if($request->has('email')){
                $data['email'] = $request->get('email');
            }
            if($request->has('dni')){
                $data['dni'] = $request->get('dni');
            }
            if($request->has('gender')){
                $data['gender'] = $request->get('gender');
            }
            if($request->has('rol')){
                $data['rol'] = $request->get('rol');
            }

            $user = $this->Transaction->find_token($token);
            $user = $this->Transaction->update($user, $request->all());

            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Actualizado Correctamente'),
                'data' => $user,
            ];

            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];

            return response()->json($response, 500);
        }
    }

    public function delete($token)
    {

        try {
            $user = $this->Transaction->find_token($token);
            $user = $this->Transaction->delete($user, ['active' => false]);
            $response = [
                'status' => 'OK',
                'code' => 200,
                'message' => __('Usuario Eliminado Correctamente'),
                'data' => $user,
            ];
            return response()->json($response, 200);
        } catch (\Exception $ex) {
            $response = [
                'status' => 'FAILED',
                'code' => 500,
                'message' => _('Ocurrio un error interno') . '.',
            ];
            return response()->json($response, 500);
        }
    }

    public function custom_message()
    {

        return [
            'name.required' => __('El nombre es requerido') . '.',
            'username.required' => __('El nombre de usuario es requerido') . '.',
            'token.required' => __('El token es requerido') . '.',
        ];

    }

}
