<?php
    
    namespace App\Http\Controllers\Back;

    use Illuminate\Routing\Controller as BaseController;
    
    class PassengerController extends BaseController {
        public function index(){
            return view('back.Passengers.index');
        }
    }
