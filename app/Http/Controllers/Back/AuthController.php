<?php
    
    namespace App\Http\Controllers\Back;
    use App\ClientCompany\Entities\ClientCompany;
    use App\Security\Entities\Role;
    use App\Security\Enums\Roles;
    use App\ShippingCompany\Collection\ShippingCompany;
    use App\Users\Entities\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

    class AuthController extends Controller {

        public function authenticate(Request $request)
        {
            $this->validate($request, [
                'dni' => 'required',
                'password' => 'required',
            ], $this->custom_message());
            try {
                $username = $request->input('dni');
                $password = $request->input('password');
                $credentials = [
                    'dni' => $username,
                    'password' => $password
                ];
                if (Auth::attempt($credentials)) {
                    $user = User::find(Auth::id());
                    $response = [
                        'status' => 'OK',
                        'title' => '¡Bienvenido!',
                        'message' => 'Bienvenido',
                    ];
                    $rol = Role::find($user->rol);
                    $permissions = $rol->permissions;
                    $permissionsArray = [];
                    foreach ($permissions as $permission){
                        $permissionsArray[$permission->id] = $permission->id;
                    }

                    session()->put('permissions',$permissionsArray);

                    return redirect()->route('back.index')->with($response);
                } else {
                    $response = [
                        'status' => 'FAILED',
                        'title' => __('¡Vaya! ¡Algo salió mal!'),
                        'message' => __('Credenciales incorrectas'),
                    ];
                }

            } catch (\Exception $exception) {
                session()->forget('permissions');
                Auth::logout();
                $response = [
                    'status' => 'FAILED',
                    'title' => __('¡Vaya! ¡Algo salió mal!'),
                    'message' => $exception->getMessage(),
                ];
            }
            return redirect()->back()->with($response);
        }


        public function login() {
            
            return view('back.auth.login');
        }
        
        public function logout() {
            session()->forget('permissions');
            Auth::logout();
            
            $response = [
                'status'  => 'OK',
                'title'   => __('¡Hasta pronto!'),
                'message' => __('Gracias por preferirnos') . '.',
            ];
            
            return redirect()->route('login')->with($response);
        }
        
        public function register(Request $request) {
            
            return view('back.auth.register');
        }
        public function custom_message()
        {
            return [
                'username.required' => __('El nombre de usuario es requerido'),
                'email.required' => __('El Correo Electrónico es requerido'),
                'password.required' => __('La Contraseña es requerida')
            ];
        }
        
    }
