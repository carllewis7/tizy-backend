<?php
    
    namespace App\Transaction\Entities;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Transaction extends Model {

        protected $table      = 'transaction';
        protected $primaryKey = 'id';
        
        protected $fillable = [
            'id',         
            'token',
            'iduserA',
            'iduserB',
            'amount',
            'type',           
            'active',
            
        ];
        public function scopeToken($query, $value) {
            return $query->where('token', $value)->first();
        }
    }
