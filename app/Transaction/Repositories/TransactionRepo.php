<?php

    namespace App\Transaction\Repositories;
    
    use App\Transaction\Entities\Transaction;
   

    class TransactionRepo {
    
        public function all() {

            $Transaction = Transaction::all();       

            return $Transaction;
        }
  
        public function find($id) {
    
            $Transaction = Transaction::find($id);

            return $Transaction;
        }  
        
        public static function findByToken($token) {
            $Transaction = Transaction::Token($token);
         
            return $Transaction;
        }      

        public function store($data) {
    
            $Transaction = new Transaction();
            $Transaction->fill($data);
            $Transaction->save();

            return $Transaction;
        }
        
        public function update($Transaction, $data) {
    
            $Transaction->fill($data);
            $Transaction->save();

            return $Transaction;
        }
        public function delete($Transaction,$data) {

            $Transaction->active = false;
            $Transaction->save();

            return $Transaction;
        }
    }
