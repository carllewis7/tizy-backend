<?php
    
    use Illuminate\Database\Seeder;
    
    class PermissionsTableSeeder extends Seeder {
        
        public function run() {
            \App\Security\Entities\Permission::create([
                'name' => 'Login',
            ]);
        }
    }
