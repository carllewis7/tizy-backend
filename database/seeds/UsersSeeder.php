<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{

    public function run()
    {
        \App\Users\Entities\User::create([
            'name' => 'Super Administrador',
            'dni' => 22222222222,
            'token' => \App\Components\UUID::v4(),
            'email' => 'root@root.root',
            'rol' => 1,
            'gender' => 2,
            'phone' => 11111111111,
            'password' => '123456',
            'active' => 1,
            'status' => 1,
        ]);
    }
}
