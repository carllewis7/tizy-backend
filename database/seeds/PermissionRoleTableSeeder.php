<?php
    
    use Illuminate\Database\Seeder;
    use App\Security\Enums\Permissions;
    use App\Security\Enums\Roles;
    
    class PermissionRoleTableSeeder extends Seeder {

        public function run() {

            \DB::table('permission_role')->insert([
                'permission_id' => Permissions::$login,
                'role_id'       => Roles::$root,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);

            \DB::table('permission_role')->insert([
                'permission_id' => Permissions::$login,
                'role_id'       => Roles::$admin,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);

            \DB::table('permission_role')->insert([
                'permission_id' => Permissions::$login,
                'role_id'       => Roles::$driver,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);

            \DB::table('permission_role')->insert([
                'permission_id' => Permissions::$login,
                'role_id'       => Roles::$passenger,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);

        }
    }
