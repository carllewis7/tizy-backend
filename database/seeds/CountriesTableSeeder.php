<?php
    
    use Illuminate\Database\Seeder;
    
    class CountriesTableSeeder extends Seeder {
        
        public function run() {
            foreach (json_decode(\Illuminate\Support\Facades\Storage::disk('file')->get('countries.json'))->countries as $item => $value) {
                \App\Core\Entities\Country::create([
                    'iso'  => $value->sortname,
                    'name' => $value->name,
                    'dni'  => (isset($value->dni) ? $value->dni : []),
                ]);
            }
            
        }
    }
