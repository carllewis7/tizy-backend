<?php
    
    use Illuminate\Database\Seeder;
    
    class CurrenciesTableSeeder extends Seeder {
        
        public function run() {
            
            \App\Core\Entities\Currency::create([
                'iso'                => 'USD',
                'name'               => 'United States Dollar',
                'symbol'             => '$',
                'thousand_separator' => ',',
                'decimal_separator'  => '.',
                'decimals'           => 2,
            ]);
        }
    }
