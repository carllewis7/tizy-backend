<?php
    
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;
    
    class CreateUsersTable extends Migration {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up() {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('dni', 20)->unique();
                $table->string('name', 50);
                $table->integer('gender');
                $table->string('token')->unique();
                $table->string('phone')->nullable();
                $table->string('email')->unique();              
                $table->integer('rol');              
                $table->string('password');
                $table->boolean('active');
                $table->boolean('status');
                $table->rememberToken();
                $table->timestamps();
            });
        }
        
        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down() {
            Schema::dropIfExists('users');
        }
    }
